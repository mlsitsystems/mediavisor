<?php

define('DS',DIRECTORY_SEPARATOR);
$bp = __DIR__;

/**
 * Закомментить при деплое
/*
 */
$devIps = array(
	'::1',
	'127.0.0.1',
	'192.168.1.34',
);
if (in_array($_SERVER['SERVER_ADDR'], $devIps)) {
	$systemPath = $bp.DS.'..'.DS.'vendor'.DS.'yiisoft'.DS.'yii'.DS.'framework';
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else
/**/

$systemPath = $bp.DS.'framework';
$appPath = $bp.DS.'app';

require_once($systemPath.DS.'yii.php');
require_once($appPath.DS.'shortcuts.php');
$app = Yii::createWebApplication($appPath.DS.'config.php');
// Добавление AppBaseUrl к значению baseUrl компонента CAssetManager
Yii::app()->getAssetManager()->setBaseUrl(Yii::app()->getRequest()->getBaseUrl().'/'.Yii::app()->getAssetManager()->getBaseUrl());
$app->run();