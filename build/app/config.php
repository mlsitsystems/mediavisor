<?php
$bp = __DIR__;
$config = array(
	'name' => 'MediaVisor',
	'basePath' => $bp,
	'defaultController' => 'main',
	'layoutPath' => $bp.DS.'layouts',
	'import' => array(
		'application.components.*',
		
		/**
		 * Тут пиздец, пришлось все перековырять, еще сырая бета.
		 * Исходный код изменен во всех этих файлах:
		 */
		'ext.nlac.NLSDownloader',
		'ext.nlac.NLSCssMerge',
		'ext.nlac.NLSUtils',
		'application.vendor.JShrink',
	),
	'components' => array(
		'assetManager' => array(
			'basePath' => 'exposed-assets',
			'baseUrl' => 'exposed-assets',
		),
		
		// Старое расширение 6.x (все работало, кроме замены url в css, поэтому пришлось перейти на новое,
		// но там тоже пиздец, читать выше)
		//'clientScript' => ($cs = include $bp.DS.'extensions'.DS.'nlsclientscript'.DS.'config.php')?$cs:array(),
		// Новое расширение 7.x (beta)
		'clientScript' => ($cs = include $bp.DS.'extensions'.DS.'nlac'.DS.'config.php')?$cs:array(),
		
		'db' => array(
			'connectionString' => 'sqlite:'.$bp.DS.'db.sqlite',
		),
	),
);
$config['params'] = array(
	'headTitleBase' => $config['name'].' - Информационно-развлекательная ресторанная система от ArchiDelivery',
);
return $config;