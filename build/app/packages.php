<?php
return array(
	'bootstrap' => array(
		'baseUrl' => 'assets/vendor/bootstrap',
		'css' => array(
			'css/bootstrap.css',
			//'css/bootstrap-theme.css',
		),
		'js' => array('js/bootstrap.js'),
		'depends' => array('jquery'),
	),
	'colorbox' => array(
		'baseUrl' => 'assets/vendor/colorbox',
		'css' => array('colorbox.css'),
		'js' => array('jquery.colorbox.js'),
		'depends' => array('jquery'),
	),
	'slick' => array(
		'baseUrl' => 'assets/vendor/slick.js',
		'css' => array(
			'slick.css',
//			'slick-theme.css',
		),
		'js' => array('slick.js'),
		'depends' => array('jquery'),
	),
	'imagesloaded' => array(
		'baseUrl' => 'assets/vendor',
		'js' => array('imagesloaded.js'),
	),
	'main' => array(
		'baseUrl' => 'assets/css',
		'css' => array('main.css'),
		'depends' => array('bootstrap'),
	),
	'frontpage' => array(
		'baseUrl' => 'assets',
		'css' => array('css/frontpage.css'),
		'js' => array('js/frontpage.js'),
		'depends' => array('main', 'slick', 'imagesloaded'),
	),
);