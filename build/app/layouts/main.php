<?php
	$bu = Yii::app()->getBaseUrl();
	Yii::app()->getClientScript()->registerPackage('main');
	
	/**
	 * Если на локалке, то подключить файл без временной метки
	 * Если на рабочем сервере, то выбрать самый свежий по временной метке в имени файла
	 */
	 /*
	$cssFileName = 'mediavisor.css';
	$jsFileName = 'mediavisor.js';
	if (!YII_DEBUG) {
		$files = array();
		$matches = array();
		$fileNameRegEx = '/^mediavisor-(\d{12})\.css$/';
		$wd = Yii::getPathOfAlias('webroot').DS.'assets'.DS.'css';
		foreach (scandir($wd) as $file)
			if (!is_dir($wd.DS.$file) && preg_match($fileNameRegEx, $file, $matches))
				$files[$matches[1]] = $file;
		ksort($files, SORT_NUMERIC);
		if (!empty($files))
		$cssFileName = array_pop($files);
		
		$files = array();
		$matches = array();
		$fileNameRegEx = '/^mediavisor-(\d{12})\.js$/';
		$wd = Yii::getPathOfAlias('webroot').DS.'assets'.DS.'js';
		foreach (scandir($wd) as $file)
			if (!is_dir($wd.DS.$file) && preg_match($fileNameRegEx, $file, $matches))
				$files[$matches[1]] = $file;
		ksort($files, SORT_NUMERIC);
		if (!empty($files))
		$jsFileName = array_pop($files);
	}
	/**/
?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<link rel="icon" type="image/x-icon" href="<?php echo $bu ?>/favicon.ico">
<title><?php echo $this->headTitle.' - '.Yii::app()->params['headTitleBase'] ?></title>
<?php if (false): ?>
<script type="text/javascript" data-main="<?php echo $bu ?>/../src/js/main.js" src="<?php echo $bu ?>/../bower_components/requirejs/require.js"></script>
<script type="text/javascript" src="<?php echo $bu ?>/assets/js/<?php echo $jsFileName ?>"></script>
<?php endif ?>
</head>

<body>
<?php echo $content ?>
</body>
</html>