<?php
class MainController extends CController
{
	//public $layout = 'admin';
	public $headTitle = '';
	
	protected function beforeRender($view)
	{
		if (!empty($this->headTitle)) {
			$this->headTitle .= ' - ';
		}
		$this->headTitle .= Yii::app()->params['headTitleBase'];
		return true;
	}
	
	public function actionIndex()
	{
		//echo Yii::app()->id;
		//$this->headTitle = 'asdas';
		$this->render('frontpage');
	}
	
	public function actionLogin()
	{
		$this->render('login');
	}
	
	public function actionRegister()
	{
		$this->render('register');
	}
}