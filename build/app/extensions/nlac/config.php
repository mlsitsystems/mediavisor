<?php
return array(
	//'class' => 'nlac\NLSClientScript',
	//'class' => 'ext\nlac\NLSClientScript',
	'class' => 'ext.nlac.NLSClientScript',
	//'defaultScriptPosition' => CClientScript::POS_HEAD,
	'packages' => require __DIR__.DS.'..'.DS.'..'.DS.'packages.php',
	
	'mergeJs' => YII_DEBUG ? false : true,
	'compressMergedJs' => true,
	'forceMergeJs' => false,
	'mergeCss' => YII_DEBUG ? false : true,
	'compressMergedCss' => true,
	'forceMergeCss' => false,
	'appVersion' => '0.0.1',
);