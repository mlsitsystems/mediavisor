<?php
	//$this->headTitle = 'Выбор точки'.' - '.Yii::app()->params['headTitleBase'];
	$bu = Yii::app()->getBaseUrl();
	Yii::app()->getClientScript()->registerPackage('frontpage');
?>
<?php /* ?>
<?php */ ?>
<div class="mv-fp-loading-screen helper-row">
	<div class="helper-cell helper-middle">
		<div class="mv-fp-loading-screen-content">
			<img class="mv-fp-loading-screen-icon" src="<?php echo $bu ?>/assets/img/preloader.gif"><br><span class="mv-fp-loading-screen-text">Загрузка</span>
		</div>
	</div>
</div>
<div class="mv-fp-bg-nest helper-row">
	<div class="helper-cell helper-middle">
		<div class="mv-fp-bg-shell">
			<img class="mv-fp-bg mv-fp-bg-desktop mv-mod-hidden" src="<?php echo $bu ?>/assets/img/fp-bg.jpg"><img class="mv-fp-bg mv-fp-bg-mobile mv-mod-hidden" src="<?php echo $bu ?>/assets/img/fp-bg-mobile.jpg">
		</div>
	</div>
</div>
<div class="mv-fp-content-nest helper-row helper-full-height helper-full-width">
	<div class="helper-cell helper-middle">
		<div class="mv-fp-content">
			<img class="mv-fp-logo mv-mod-hidden" src="<?php echo $bu ?>/assets/img/fp-logo.png"><img 
			class="mv-fp-sublogo mv-mod-hidden" src="<?php echo $bu ?>/assets/img/fp-sublogo.png">
			<div class="mv-fp-screens mv-mod-hidden">
				<div class="mv-fp-screens-slider-clip">
					<div class="mv-fp-screens-slider">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-1.png">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-2.png">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-3.png">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-1.png">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-2.png">
						<img src="<?php echo $bu ?>/assets/img/fp-screen-3.png">
					</div>
				</div>
			</div>
			<div class="mv-fp-text mv-mod-hidden">
				Информационно-развлекательная<br>
				ресторанная система
			</div>
			<div class="mv-fp-buttons mv-mod-hidden">
				<a class="mv-button mv-button-big mv-button-orange" href="<?php echo $this->createUrl('login') ?>">
					Вход
				</a><a class="mv-button mv-button-big" href="<?php echo $this->createUrl('register') ?>">
					Регистрация
				</a>
			</div>
		</div>
	</div>
</div>