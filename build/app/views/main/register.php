<?php
	$bu = Yii::app()->getBaseUrl();
	$this->headTitle = 'Регистрация';
	Yii::app()->getClientScript()->registerPackage('main');
?>
<img src="<?php echo $bu ?>/assets/img/fp-logo.png" style="width:40rem; display:block; margin:8rem auto -8rem;">
<div class="mv-user-box panel panel-default">
	<div class="panel-heading">
		<h3 class="mv-user-box-title panel-title">Регистрация</h3>
	</div>
	<div class="mv-user-box-body panel-body">
		<form>
			<div class="form-group">
				<label for="exampleInputEmail1">Адрес сервера</label>
				<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Адрес сервера">
				<span class="help-block">Пример: <em>https://92.137.91.17/archidelivery</em></span>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Основной E-mail</label>
				<input type="email" class="form-control" id="exampleInputPassword1" placeholder="Введите пароль">
				
			</div>
			<div class="helper-center" style="margin-top:2rem">
				<button type="button" class="mv-button mv-button-orange">Зарегистрироваться</button>
			</div>
		</form>
	</div>
</div>