<?php
	$bu = Yii::app()->getBaseUrl();
	$this->headTitle = 'Вход';
	Yii::app()->getClientScript()->registerPackage('main');
?>
<img src="<?php echo $bu ?>/assets/img/fp-logo.png" style="width:40rem; display:block; margin:8rem auto -8rem;">
<div class="mv-user-box panel panel-default">
	<div class="panel-heading">
		<h3 class="mv-user-box-title panel-title">Войти</h3>
	</div>
	<div class="mv-user-box-body panel-body">
		<form>
			<div class="form-group">
				<label for="exampleInputEmail1">E-mail</label>
				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="E-mail пользователя">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Пароль</label>
				<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Введите пароль">
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" checked> Запомнить меня
				</label>
			</div>
			<div class="helper-center" style="margin-top:2rem">
				<button type="button" class="mv-button mv-button-orange">Войти</button>
			</div>
		</form>
	</div>
</div>
