

$(function(){
	
	// Изображения загружены; Цепочка анимаций
	$('body').imagesLoaded(function() {
		window.setTimeout(function() {
			$('.mv-fp-loading-screen-content').addClass('mv-mod-hidden');
			window.setTimeout(function() {
				$('.mv-fp-loading-screen').remove();
				$('.mv-fp-logo').removeClass('mv-mod-hidden');
				window.setTimeout(function() {
					$('.mv-fp-sublogo').removeClass('mv-mod-hidden');
					window.setTimeout(function() {
						$('.mv-fp-bg').removeClass('mv-mod-hidden');
					}, 1000);
					window.setTimeout(function() {
						$('.mv-fp-screens').removeClass('mv-mod-hidden');
						window.setTimeout(function() {
							$('.mv-fp-text').removeClass('mv-mod-hidden');
							$('.mv-fp-screens-slider').slick('slickPlay');
							window.setTimeout(function() {
								$('.mv-fp-screens-slider').slick('slickPause');
							}, 7000);
							window.setTimeout(function() {
								$('.mv-fp-buttons').removeClass('mv-mod-hidden');
							}, 500);
						}, 1000);
					}, 2300);
				}, 800);
			}, 700);
		}, 2300);
		
		// Карусель экранов
		$('.mv-fp-screens-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			centerMode: true,
			autoplaySpeed: 2000,
			prevArrow: '<button class="mv-fp-screens-slider-prev"><span class="mv-fp-screens-slider-prev-inner"></span></button>',
			nextArrow: '<button class="mv-fp-screens-slider-next"><span class="mv-fp-screens-slider-next-inner"></span></button>',
			appendArrows: $('.mv-fp-screens'),
			//speed: 500,
			//fade: true,
			//cssEase: 'linear'
		});
	});
	
});