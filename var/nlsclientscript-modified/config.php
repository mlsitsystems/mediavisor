<?php
return array(
	'class' => 'ext.nlsclientscript.NLSClientScript',
	'defaultScriptPosition' => CClientScript::POS_HEAD,
	'packages' => require __DIR__.DS.'..'.DS.'..'.DS.'packages.php',
	
	/**
	 * @param string $includePattern
	 * a javascript regex eg. '/\/scripts/' - if set, only the matched URLs will be filtered, defaults to null
	 * (can be set to string 'null' also to ignore it)
	 */
	'includePattern' => null,

	/**
	 * @param string $excludePattern
	 * a javascript regex eg. '/\/raw/' - if set, the matched URLs won't be filtered, defaults to null
	 * (can be set to string 'null' also to ignore it)
	 */
	'excludePattern' => null,

	/**
	 * @param boolean $mergeJs
	 * merge or not the registered script files, defaults to false
	 */
	'mergeJs' => YII_DEBUG ? false : true,

	/**
	 * @param boolean $compressMergedJs
	 * minify or not the merged js file, defaults to false
	 */
	'compressMergedJs' => true,

	/**
	 * @param boolean $mergeCss
	 * merge or not the registered css files, defaults to false
	 */
	'mergeCss' => YII_DEBUG ? false : true,

	/**
	 * @param boolean $compressMergeCss
	 * minify or not the merged css file, defaults to false
	 */
	'compressMergedCss' => true,

	/**
	 * @param int $mergeAbove
	 * only merges if there are more than mergeAbove file registered to be included at a position
	 */
	'mergeAbove' => 1,

	/**
	 * @param string $mergeJsExcludePattern
	 * regex for php. the matched URLs won't be filtered
	 */
	'mergeJsExcludePattern' => null,

	/**
	 * @param string $mergeJsIncludePattern
	 * regex for php. the matched URLs will be filtered
	 */
	'mergeJsIncludePattern' => null,

	/**
	 * @param string $mergeCssExcludePattern
	 * regex for php. the matched URLs won't be filtered
	 */
	'mergeCssExcludePattern' => null,

	/**
	 * @param string $mergeCssIncludePattern
	 * regex for php. the matched URLs will be filtered
	 */
	'mergeCssIncludePattern' => null,

	/**
	 * @param boolean $mergeIfXhr
	 * if true then js files will be merged even if the request rendering the view is ajax
	 * (if $mergeJs and $mergeAbove conds are satisfied)
	 * defaults to false - no js merging if the view is requested by ajax
	 */
	'mergeIfXhr' => false,
	
	/**
	 * @param string $resMap2Request
	 * code of a js function, prepares a get url by adding the script url hashes already in the dom
	 * (has effect only if mergeIfXhr is true)
	 */
	'resMap2Request' => 'function(url){if (!url.match(/\?/))url += "?";return url + "&nlsc_map=" + $.nlsc.smap();};',

	/**
	 * @param string $serverBaseUrl
	 * used to transform relative urls to absolute (for CURL)
	 * you may define the url of the DOCROOT on the server (defaults to a composed value from the $_SERVER members) 
	 */
	'serverBaseUrl' => '',

	/**
	 * @param string $appVersion
	 * Optional, version of the application.
	 * If set to not empty, will be appended to the merged js/css urls (helps to handle cached resources).
	 */
	'appVersion' => '0.0.1',

	/**
	 * @param int $curlTimeOut
	 * see http://php.net/manual/en/function.curl-setopt.php
	 */
	'curlTimeOut' => 10,

	/**
	 * @param int $curlConnectionTimeOut
	 * see http://php.net/manual/en/function.curl-setopt.php
	 */
	'curlConnectionTimeOut' => 10,
);