module.exports = function(grunt) {
	var timestamp = grunt.template.today("yyyymmddhhMM");
	grunt.config.init({
		
		/** -------------------------------------------
		 *  REQUIREJS
		 * -------------------------------------------- */
		requirejs: {
			compile: {
				options: {
					baseUrl: 'src/js',
					paths: {
						build: '../../build',
						jquery: '../../bower_components/jquery/dist/jquery'
					},
					name: 'main',
					out: 'runtime/' + timestamp + '.js'
				}
			}
		},
		
		/** -------------------------------------------
		 *  CLEAN
		 * -------------------------------------------- */
		clean: {
			vendor: {
				src: ['build/vendor/*'],
				dot: true
			},
		},
		
		/** -------------------------------------------
		 *  COPY
		 * -------------------------------------------- */
		copy: {
			requirejs: {
				files: {
					'build/js/require.js': 'bower_components/requirejs/require.js'
				}
			},
			bower: {
				files: [
					// Bootstrap
					{ expand:true, cwd:'bower_components/bootstrap/dist/', src:'**', dest:'build/assets/vendor/bootstrap/' },
					// Spin.js
					{ expand:true, cwd:'bower_components/spinjs/', src:'**', dest:'build/assets/vendor/spinjs/' },
					// Slick.js
					{ expand:true, cwd:'bower_components/slick.js/slick/', src:'**', dest:'build/assets/vendor/slick.js/' },
					// Colorbox
					{ src:'bower_components/colorbox/jquery.colorbox.js', dest:'build/assets/vendor/colorbox/jquery.colorbox.js' },
					{ expand:true, cwd:'bower_components/colorbox/example1/', src:'**', dest:'build/assets/vendor/colorbox/' },
					// ImagesLoaded
					{ src:'bower_components/imagesloaded/imagesloaded.pkgd.js', dest:'build/assets/vendor/imagesloaded.js' },
					
				]
			}
		},
		
		/** -------------------------------------------
		 *  CONCAT
		 * -------------------------------------------- */
		concat: {
			js: {
				options: {
					stripBanners: true,
					banner: '/*! MediaVisor - Скрипты - ' + timestamp + ' */'+"\n",
				},
				src: [
					'runtime/require.min.js',
					'runtime/' + timestamp + '.js',
				],
				dest: 'build/js/mediavisor-' + timestamp + '.js',
			},
			cssDev: {
				src: [
					'bower_components/bootstrap/dist/css/bootstrap.css',
					'bower_components/bootstrap/dist/css/bootstrap-theme.css',
					'runtime/app.css',
				],
				dest: 'build/css/mediavisor.css',
			},
			cssBuild: {
				options: {
					stripBanners: true,
					banner: '/*! MediaVisor - Стили - ' + timestamp + ' */'+"\n",
				},
				src: [
					'bower_components/bootstrap/dist/css/bootstrap.min.css',
					'bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
					'runtime/app.css',
				],
				dest: 'build/css/mediavisor-' + timestamp + '.css',
			}
		},
		
		/** -------------------------------------------
		 *  CSSMIN
		 * -------------------------------------------- */
		cssmin: {
			dev: {
				files: {}
			}
		},
		
		/** -------------------------------------------
		 *  UGLIFY
		 * -------------------------------------------- */
		uglify: {
			options: {
				mangle: false,
				//banner: '/*! <%= grunt.template.today("dd.mm.yyyy") %> */'+"\n",
			},
			requirejs: {
				src: 'bower_components/requirejs/require.js',
				dest: 'runtime/require.min.js',
			}
		},
		
		/** -------------------------------------------
		 *  SASS
		 * -------------------------------------------- */
		sass: {
			options: {
				sourcemap: 'none',
				style: 'expanded'
			},
			main: {
				src: 'src/css/main.scss',
				dest: 'build/assets/css/main.css'
			},
			frontpage: {
				src: 'src/css/frontpage.scss',
				dest: 'build/assets/css/frontpage.css'
			},
		},
		
		/** -------------------------------------------
		 *  IMAGEMIN
		 * -------------------------------------------- */
		imagemin: {
			img: {
				files: [{
					expand: true,
					cwd: 'src/img/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'build/assets/img/'
				}],
			},
			svg: {
				options: {
					svgoPlugins: [
						{ cleanupAttrs: true },
						{ cleanupEnableBackground: true },
						{ cleanupIDs: true },
						{ cleanupNumericValues: true },
						{ collapseGroups: true },
						{ convertColors: true },
						{ convertPathData: true },
						{ convertShapeToPath: true },
						{ convertStyleToAttrs: true },
						{ convertTransform: true },
						{ mergePaths: true },
						{ moveElemsAttrsToGroup: true },
						{ moveGroupAttrsToElems: true },
						{ removeComments: true },
						{ removeDesc: true },
						{ removeDoctype: true },
						{ removeEditorsNSData: true },
						{ removeEmptyAttrs: true },
						{ removeEmptyContainers: true },
						{ removeEmptyText: true },
						{ removeHiddenElems: true },
						{ removeMetadata: true },
						{ removeNonInheritableGroupAttrs: true },
						{ removeRasterImages: true },
						{ removeTitle: true },
						{ removeUnknownsAndDefaults: true },
						{ removeUnusedNS: true },
						{ removeUselessStrokeAndFill: true },
						{ removeViewBox: false },
						{ removeXMLProcInst: true },
						{ sortAttrs: true },
						{ transformsWithOnePath: true },
					]
				},
				files: [{
					expand: true,
					cwd: 'assets/src/img/svg',
					src: ['*.svg'],
					dest: 'assets/src/img/svg/optimized',
					ext: '.optimized.png',
				}],
			}
		},
		
		/** -------------------------------------------
		 *  SVGSTORE
		 * -------------------------------------------- */
		svgstore: {
			options: {
				prefix : '',
			},
			default: {
				files: {
					'sources/backend/images/svg/merged/backend.merged.svg': ['sources/backend/images/svg/optimized/*'],
				},
			},
		},
		
		/** -------------------------------------------
		 *  WATCH
		 * -------------------------------------------- */
		watch: {
			options: {
				spawn: false,
			},
			css_main: {
				files: [
					'src/css/variables.scss',
					'src/css/mixins.scss',
					'src/css/helpers.scss',
					'src/css/app.scss',
					'src/css/main.scss',
				],
				tasks: ['sass:main'],
			},
			css_frontpage: {
				files: [
					'src/css/variables.scss',
					'src/css/mixins.scss',
					'src/css/frontpage.scss',
				],
				tasks: ['sass:frontpage'],
			},
			img: {
				files: ['src/img/*.{png,jpg,gif}', 'src/img/**/*.{png,jpg,gif}'],
				tasks: ['build:img']
			}
		},
		
		/** -------------------------------------------
		 *  CONCURRENT
		 * -------------------------------------------- */
		concurrent: {
			options: {
				logConcurrentOutput: true,
			},
			default: [
				'watch:css_main',
				'watch:css_frontpage',
				'watch:img',
			],
		},
	});
	
	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-concurrent');
	/*
	grunt.loadNpmTasks('grunt-svgstore');
	*/

	grunt.registerTask('default', ['concurrent']);
	grunt.registerTask('build', [
		'build:css',
		//'build:js',
		'build:img',
	]);
	grunt.registerTask('build:css', ['sass:main', 'sass:frontpage']);
	//grunt.registerTask('build:js', ['requirejs', 'uglify:requirejs', 'concat:js']);
	grunt.registerTask('build:img', ['imagemin:img']);
	grunt.registerTask('update', [
		'clean:vendor',
		'copy:bower',
	]);
};